#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

import argparse
import re
from sys import argv,exit,stdin
from os import getenv as osgetenv
from os import path,stat
from os import linesep as linesep
#from time import gmtime
from datetime import datetime as dt


""" Generate a suitable ks file from arguments supplied
Expects stdin to be supplied with a set of lines suitable
for making up the section 'Disk partitioning information'
cat kspart.txt | python2 ./ks_graphical_sda_desktopx.py --rootpass '$60' --username 'user1' --userpass '$61' --textgraphical 'text'
# Because textgraphical is an optional argument can use the slightly shorter example below for graphical
cat kspart.txt | python2 ./ks_graphical_sda_desktopx.py --rootpass '$60' --username 'user1' --userpass '$61'
"""

PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
PYVERBOSITY = 1
if PYVERBOSITY_STRING is None:
	pass
else:
	try:
		PYVERBOSITY = int(PYVERBOSITY_STRING)
	except:
		pass
#print(PYVERBOSITY)

""" Next verbosity_global = 1 is the most likely outcome
unless the user specifically overrides it by setting
the environment variable PYVERBOSITY
"""
if PYVERBOSITY is None:
	verbosity_global = 1
elif 0 == PYVERBOSITY:
	# 0 from env means 0 at global level
	verbosity_global = 0
elif PYVERBOSITY > 1:
	# >1 from env means same at global level
	verbosity_global = PYVERBOSITY
else:
	verbosity_global = 1


#from string import ascii_letters,punctuation,printable
from string import printable
SET_PRINTABLE = set(printable)

progfull = argv[0].strip()
from os import path
progbase = path.basename(progfull)
progname = progbase.split('.')[0]
progdir = path.dirname(progbase)

ISOCOMPACT_STRFTIME='%Y%m%dT%H%M%S'

ADESC="""Kickstart from combination of stdin (part) and supplied arguments."""

COLON=chr(58)
SPACER=chr(32)
HASHER=chr(35)
DASH=chr(45)
PARENTH_OPEN = chr(40)
PARENTH_CLOSE = chr(41)
# Above are () and next we define {}
BRACES_OPEN = chr(123)
BRACES_CLOSE = chr(125)


RE_PART_FSTYPE_ONPART=re.compile("part.*{0}{0}fstype=.*{0}{0}onpart=".format(DASH))
RE_VOLGROUP=re.compile("volgroup\s+\w+")
RE_VGNAME=re.compile("logvol\s+/.*{0}{0}vgname".format(DASH))
RE_LEVELRAID=re.compile("{0}{0}level=RAID\d+".format(DASH))

KS_TEMPLATE="""
#version=DEVEL
# System authorization information
auth --enableshadow --passalgo=sha512
# Use CDROM installation media
cdrom
# Use {textgraphical} install
{textgraphical}
# Run the Setup Agent on first boot
firstboot --enable
ignoredisk --only-use=sda
# Keyboard layouts
keyboard --vckeymap=gb --xlayouts='gb'
# System language
lang en_GB.UTF-8

# Network information
network  --bootproto=dhcp --device=eno1 --onboot=off --ipv6=auto --no-activate
network  --hostname=localhost.localdomain

# Root password
rootpw --iscrypted {password6root}
# System services
services --enabled=chronyd,sshd --disabled="firewalld"
# System timezone
timezone Europe/London --isUtc
user --groups=wheel --name={w2user} --password={password6user} --iscrypted --gecos="{w2user}"
# X Window System configuration information
xconfig  --startxonboot
# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=sda
# Partition clearing information
clearpart --none --initlabel
# Disk partitioning information
{part_multiline}

%packages
@^gnome-desktop-environment
@backup-client
@base
@core
@desktop-debugging
@development
@dial-up
@directory-client
@fonts
@gnome-desktop
@guest-agents
@guest-desktop-agents
@input-methods
@internet-applications
@internet-browser
@java-platform
@multimedia
@network-file-system-client
@networkmanager-submodules
@office-suite
@print-client
@security-tools
@smart-card
@x11
chrony
kexec-tools
openssh-clients
openssh-server

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

%anaconda
pwpolicy root --minlen=6 --minquality=50 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=50 --notstrict --nochanges --notempty
pwpolicy luks --minlen=6 --minquality=50 --notstrict --nochanges --notempty
%end
"""


def process_stdin_lines(lines_as_list):
	""" if all the lines match() the required regex
	then we populate lines array
	Typical non raid setup that involves lvm would have part+volgroup+logvol (vgname)
	Typical RAID will have level=RAID lines
	"""
	lines = []
	line_count = 0
        line_matched_count = 0
	for line in lines_as_list:
		line_count+=1
		line_stripped = None
		if RE_PART_FSTYPE_ONPART.match(line):
			line_matched_count+=1
			lines.append(line.rstrip())
		elif RE_VOLGROUP.match(line):
			line_matched_count+=1
			lines.append(line.rstrip())
		elif RE_VGNAME.match(line):
			line_matched_count+=1
			lines.append(line.rstrip())
		elif RE_LEVELRAID.match(line):
			""" Likely that our partition plan is defining the
			paired [IDE] drives (and spares) manually and we would 
			not be just using /dev/sda but also its relation sdb or sdc
			Might find this clashes with any ignoredisk line in template!
			This sort of --level=RAID1 setup is not normally required
			for proper server hardware using SAS drive bays
			"""
			line_matched_count+=1
			lines.append(line.rstrip())
		else:
			line_stripped=line.strip()

		if line_stripped is None:
			# line already counted in matched_count
			continue

		if line_stripped.startswith(HASHER):
			if set(line_stripped).issubset(SET_PRINTABLE):
				line_matched_count+=1
				lines.append(line)
			continue
		# Have not counted the line so count will be short
	
	if len(lines) < line_matched_count:
		# Unexpected
		lines = []
	elif len(lines) > line_matched_count:
		# Some lines did not get counted 'matched'
		lines = []
	else:
		# all looks good
		pass
			
	return lines


if __name__ == '__main__':


	args_all = argv[1:]
	try:
		args_all_joined = SPACER.join(args_all)
	except:
		args_all_joined = args_all

	par = argparse.ArgumentParser(description=ADESC)
	# Avoid type=bool or similar as that kind of processing is after args assigned here.
	par.add_argument('--rootpass', default='',required=True,
				action='store',
				dest='rootpass',
				help="hashed password for root $6...")
	par.add_argument('--username', default='',required=True,
				action='store',
				dest='username',
				help="Name of the regular (non-root) user that can login")
	par.add_argument('--userpass', default='',required=True,
				action='store',
				dest='userpass',
				help="hashed password for user $6...")
	par.add_argument('--textgraphical', default='graphical',
				action='store',
				dest='textgraphical',
				help="'text' if you want curses (servers) or 'graphical' for desktops")
	args = par.parse_args()

	#installonshutdown = false_unless_true_lowercase(args.installonshutdown)
	rootpass = args.rootpass.strip()
	username = args.username.strip()
	userpass = args.userpass.strip()
	textgraphical = args.textgraphical.strip()
	if len(textgraphical) < 4:
		textgraphical = 'graphical'
	stdin_iter = iter(stdin.readline, '')
	plines = process_stdin_lines(stdin_iter)

	if len(plines) > 1:
		krc = 0
	else:
		krc = 110

	if verbosity_global > 1:
		print("{0} matching lines from stdin lines".format(len(plines)))


	ks_dict = {'password6root': rootpass,
		'w2user': username,
		'password6user': userpass,
		'textgraphical': textgraphical,
		'part_multiline': linesep.join(plines),
		}

	ks = KS_TEMPLATE.format(**ks_dict)
	for line in ks.split(linesep):
		print(line)

""" Trivial example to pipe into stdin to get you started:
part /boot --fstype="ext4" --onpart=sda2 --label=boot2
part swap --fstype="swap" --noformat --onpart=sda3
part / --fstype="xfs" --onpart=sda9 --label=centroot --encrypted
"""

